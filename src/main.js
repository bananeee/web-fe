import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'ant-design-vue/dist/antd.css'
import './assets/styles/index.css'
import * as antd from 'ant-design-vue'
import Logger from './myLoger'
import CKEditor from 'ckeditor4-vue'
Vue.config.productionTip = false
Vue.use(antd)
Vue.use(Logger)
Vue.use(CKEditor)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
