import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '../views/Dashboard'
import authorizeRoutes from '../modules/authorize/router'
import accountRoutes from '../modules/account/router'
import UMLtRoutes from '../modules/plaintuml-editor/router'
import OnlineLearingRoutes from '../modules/online-learing/router'
import QuizRoutes from '../modules/quizz/router'
// import store from '../store/index'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard,
    children: [],
    meta: { requireAuth: true, adminAuth: true, title: 'Home Page - Example App' }
  },
  {
    path: '/account',
    name: 'users',
    component: Dashboard,
    children: accountRoutes,
    meta: { requireAuth: true, adminAuth: true }
  },
  {
    path: '/courses',
    name: 'Course',
    component: Dashboard,
    children: OnlineLearingRoutes,
    meta: { requireAuth: true, adminAuth: true }
  },
  {
    path: '/quiz',
    name: 'quiz',
    component: Dashboard,
    children: QuizRoutes,
    meta: { requireAuth: true, adminAuth: true }
  },
  ...UMLtRoutes,
  ...authorizeRoutes,

  // ...accountRoutes
  { path: '*', redirect: '/' }
]

const router = new VueRouter({
  mode: 'history',
  // base: process.env.BASE_URL,
  routes
})

router.beforeEach(async (to, from, next) => {
  const isToken = !(localStorage.getItem('token') === null)
  console.log(isToken)
  if (isToken) {
    if (to.name === 'Login') {
      next({ name: 'Dashboard' })
    } else {
      next()
    }
  } else {
    if (to.name === 'Login') {
      next()
    } else {
      next({ name: 'Login' })
    }
  }
})
export default router
