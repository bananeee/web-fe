import repository from './base_repository'

const resource = '/roles'
export default {
  getListRole (params) {
    // const {} = params
    return repository.get(`${resource}`, { params })
  },
  createRole (payload) {
    const { code, name, description } = payload
    return repository.post(`${resource}`, { code, name, description })
  },
  updateRoleByID (id, payload) {
    const { code, name, description } = payload
    return repository.put(`${resource}/${id}`, { code, name, description })
  },
  updateStatusRoleByID (id, payload) {
    const { status } = payload
    return repository.put(`${resource}/${id}/status`, { status })
  },

  getRoleByID (id) {
    return repository.get(`${resource}/${id}`)
  },
  addAPIInRoleByID (id, payload) {
    const { apis } = payload
    return repository.put(`${resource}/${id}/apis`, { apis })
  },
  remoteAPIInRoleByID (id, payload) {
    const { apis } = payload
    return repository.delete(`${resource}/${id}/apis`, { data: { apis } })
  }

}
