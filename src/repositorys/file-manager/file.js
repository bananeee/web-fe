import repository from './base_repository'

const resource = '/files'
export default {
  uploadFile (file) { // const {} = params
    const formData = new FormData()
    formData.append('fileName', file)
    return repository.post(`${resource}`, formData)
  },
  getInfoFile (id) {
    return repository.get(`${resource}/${id}`)
  }
}
