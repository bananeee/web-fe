import axios from 'axios'

const httpClient = axios.create({
  baseURL: '/api/v1/file',
  headers: {
    'Content-Type': 'multipart/form-data',
    // anything you want to add to the headers
    Authorization: localStorage.getItem('token')
  }
})

export default httpClient
