import axios from 'axios'

const httpClient = axios.create({
  baseURL: '/api/v1/notification',
  headers: {
    'Content-Type': 'application/json',
    // anything you want to add to the headers
    Authorization: localStorage.getItem('token')
  }
})

export default httpClient
