import repository from './base_repository'

const resource = '/notifications'
export default {
  createNotication (payload) {
    return repository.post(`${resource}`, payload)
  },
  getNotifications (payload) {
    return repository.get(`${resource}`, payload)
  }

}
