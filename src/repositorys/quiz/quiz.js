import repository from './base_repository'

const resource = '/quizzes'
export default {
  getListQuiz (params) {
    // const {} = params
    return repository.get(`${resource}`, { params })
  },
  getQuiz (id) {
    // const {} = params
    return repository.get(`${resource}/${id}`)
  },
  createQuiz (payload) {
    return repository.post(`${resource}`, payload)
  }
}
