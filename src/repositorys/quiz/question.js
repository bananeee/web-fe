import repository from './base_repository'

const resource = '/questions'
export default {
  getListQuiz (params) {
    // const {} = params
    return repository.get(`${resource}`, { params })
  },
  getQuiz (id) {
    // const {} = params
    return repository.get(`${resource}/${id}`)
  },
  createQuestion (payload) {
    // const {} = params
    return repository.post(`${resource}`, payload)
  }
}
