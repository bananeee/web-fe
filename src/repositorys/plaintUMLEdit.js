import axios from 'axios'

const plantUMlBase = ''

const httpClient = axios.create({
  baseURL: plantUMlBase,
  headers: {
    'Content-Type': 'multipart/form-data',
    Cookie: 'csrftoken=R5vCOgWbuyeOvhHtOYko4Zz1vboUJlkcqzq4gjJTwpcjs6rjX8EA2lphrG3XvuIg'
  }

})
httpClient.defaults.xsrfCookieName = 'csrftoken=R5vCOgWbuyeOvhHtOYko4Zz1vboUJlkcqzq4gjJTwpcjs6rjX8EA2lphrG3XvuIg'
export default {
  createUMl (code) {
    // const a = {
    //   'Content-Type': 'multipart/form-data',
    //   Cookie: 'csrftoken=R5vCOgWbuyeOvhHtOYko4Zz1vboUJlkcqzq4gjJTwpcjs6rjX8EA2lphrG3XvuIg'
    // }
    var bodyFormData = new FormData()
    bodyFormData.append('text', code)
    return httpClient.post('/form', bodyFormData)
  }
}
