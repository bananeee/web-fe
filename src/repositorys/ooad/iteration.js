import repository from '@/repositorys/ooad/base_ooad_repository'

const resource = '/iterations'
export default {
  getListIteration (params) {
    // const {class} = params
    return repository.get(`${resource}`, { params })
  },
  createIteration (payload) {
    // eslint-disable-next-line camelcase
    const { name, start_time, end_time, group_id, input, objective } = payload
    return repository.post(`${resource}`, { name, start_time, end_time, group_id, input, objective })
  },

  addFileInIteration (id, payload) {
    // eslint-disable-next-line camelcase
    const { files } = payload
    return repository.put(`${resource}/${id}/files`, { files })
  }
}
