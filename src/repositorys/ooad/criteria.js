import repository from '@/repositorys/ooad/base_ooad_repository'

const resource = '/criterias'
export default {
  getListCriteria (params) {
    // const {class} = params
    return repository.get(`${resource}`, { params })
  },
  createCriteria (payload) {
    // eslint-disable-next-line camelcase
    const { factor, class_id, name } = payload
    return repository.post(`${resource}`, { factor, class_id, name })
  },
  updateCriteria (id, payload) {
    // eslint-disable-next-line camelcase
    const { name, description, class_id } = payload
    return repository.put(`${resource}/${id}`, { name, description, class_id })
  },
  updateStatusCriteria (id, status) {
    return repository.put(`${resource}/${id}/status`, { status })
  }
}
