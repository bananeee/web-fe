import repository from '@/repositorys/ooad/base_ooad_repository'

const resource = '/groups'
export default {
  getListGroup (params) {
    // const {class} = params
    return repository.get(`${resource}`, { params })
  },
  createGroup (payload) {
    // eslint-disable-next-line camelcase
    const { name, class_id, topic_id } = payload
    return repository.post(`${resource}`, { name, class_id, topic_id })
  },
  getGroupByID (id) {
    // eslint-disable-next-line camelcase
    return repository.get(`${resource}/${id}`)
  },
  updateGroup (id, payload) {
    // eslint-disable-next-line camelcase
    const { name, class_id, topic_id } = payload
    return repository.put(`${resource}/${id}`, { name, class_id, topic_id })
  },
  getGroupByStudent () {
    // eslint-disable-next-line camelcase
    return repository.get(`${resource}/self`)
  },
  updateStatusTopicByID (id, status) {
    return repository.put(`${resource}/${id}/status`, { status })
  },
  addStudentInGroupByID (id, payload) {
    // eslint-disable-next-line camelcase
    const { students } = payload
    return repository.put(`${resource}/${id}/students`, { students })
  },

  addScoreInGroupByID (id, payload) {
    // eslint-disable-next-line camelcase
    const { criteria_id, score } = payload
    return repository.put(`${resource}/${id}/scores`, { criteria_id, score })
  }
}
