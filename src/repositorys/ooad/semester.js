import repository from '@/repositorys/ooad/base_ooad_repository'

const resource = '/semesters'
export default {
  getListSemesters (params) {
    // const {} = params
    return repository.get(`${resource}`, { params })
  },
  createSemester (payload) {
    // eslint-disable-next-line camelcase
    const { name, end_time, start_time } = payload
    return repository.post(`${resource}`, { name, end_time, start_time })
  },
  updateSemesterByID (id, payload) {
    // eslint-disable-next-line camelcase
    const { name, end_time, start_time } = payload
    return repository.put(`${resource}/${id}`, { name, end_time, start_time })
  },
  updateStatusSemesterByID (id, status) {
    return repository.put(`${resource}/${id}/status`, { status })
  }

}
