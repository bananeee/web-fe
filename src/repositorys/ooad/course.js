import repository from '@/repositorys/ooad/base_ooad_repository'

const resource = '/classes'
export default {

  getListCourse (params) {
    const { page } = params
    return repository.request(`${resource}`, { page })
  },
  createCourse (payload) {
    // eslint-disable-next-line camelcase
    const { name, semester_id, location, teacher_code, weekday, end_lesson, start_lesson } = payload
    return repository.post(`${resource}`, {
      name,
      semester_id,
      location,
      teacher_code,
      weekday,
      end_lesson,
      start_lesson
    })
  },
  updateClassByID (id, payload) {
    // eslint-disable-next-line camelcase
    const { name, semester_id, location, teacher_code, weekday, end_lesson, start_lesson } = payload
    return repository.put(`${resource}/${id}`, {
      name,
      semester_id,
      location,
      teacher_code,
      weekday,
      end_lesson,
      start_lesson
    })
  },
  getClassByID (id) {
    // const {} = params
    return repository.get(`${resource}/${id}`)
  },
  getTopicInClassByID (id) {
    // const {} = params
    return repository.get(`${resource}/${id}/topics`)
  },
  updateStatusClassByID (id, payload) {
    const { status } = payload
    return repository.put(`${resource}/${id}/status`, { status })
  },
  addStudentInClass (id, students) {
    return repository.put(`${resource}/${id}/students`, { students: students })
  },
  removeStudentInClass (id, students) {
    return repository.delete(`${resource}/${id}/students`, { students: students })
  }

}
