import repository from '@/repositorys/ooad/base_ooad_repository'

const resource = '/students'
export default {
  getListStudent (params) {
    // const {} = params
    return repository.get(`${resource}`, { params })
  }
}
