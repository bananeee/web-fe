import repository from '@/repositorys/ooad/base_ooad_repository'

const resource = '/topics'
export default {
  getListTopic (params) {
    // const {} = params
    return repository.get(`${resource}`, { params })
  },
  createTopic (payload) {
    // eslint-disable-next-line camelcase
    const { name, description, class_id } = payload
    return repository.post(`${resource}`, { name, description, class_id })
  },
  updateTopicByID (id, payload) {
    // eslint-disable-next-line camelcase
    const { name, description, class_id } = payload
    return repository.put(`${resource}/${id}`, { name, description, class_id })
  },
  updateStatusTopicByID (id, status) {
    return repository.put(`${resource}/${id}/status`, { status })
  }
}
