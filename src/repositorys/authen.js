import repository from './base_repository'

const resource = '/login'
export default {
  login (payload) {
    const { username, password } = payload
    return repository.post(`${resource}`, { username, password })
  }
}
