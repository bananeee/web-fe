import repository from './base_repository'

const resource = '/users'
export default {
  getListUser (params) {
    // const {} = params
    return repository.get(`${resource}`, { params })
  },
  loginWithToken () {
    return repository.post(`${resource}/login/withtoken`)
  },
  updateStatusUserByID (id, payload) {
    const { status } = payload
    return repository.put(`${resource}/${id}/status`, { status })
  },
  resetPasswordUserByID (id, payload) {
    const { status } = payload
    return repository.put(`${resource}/${id}/password`, { status })
  },
  getUserByID (id) {
    return repository.get(`${resource}/${id}`)
  },
  createUser (payload) {
    // const {} = params
    const { avatar, role, username, fullName, password, email } = payload
    return repository.post(`${resource}`, { avatar, role, username, full_name: fullName, password, email, code: username })
  },
  updateUserByID (id, payload) {
    // const {} = params
    const { avatar, role, username, fullName, email } = payload
    return repository.put(`${resource}/${id}`, { avatar, role, username, full_name: fullName, email })
  }
}
