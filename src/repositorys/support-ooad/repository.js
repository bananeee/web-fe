import repository from './base_repository'

export default {
  getProductsByTags (query) {
    const { tag } = query
    return repository.get('/products/products-by-tags', { tag })
  },
  getListCourseOffering (path, query, body) {
    // const { code } = path
    const { a, b, c } = query
    return repository.post('/test/swagger', { a, b, c }, body)
  }

}
