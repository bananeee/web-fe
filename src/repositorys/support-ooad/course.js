import repository from './base_repository'

const resource = '/course-offerings'
export default {
  getListCourse (params) {
    // const {} = params
    return repository.get(`${resource}`, { params })
  },
  getStudentInCourse (id) {
    // const {} = params
    return repository.get(`${resource}/${id}/students`)
  },
  getProjectInCourse (id) {
    return repository.get(`${resource}/${id}/projects`)
  },
  getTeamInCourse (id) {
    return repository.get(`${resource}/${id}/teams`)
  }
}
