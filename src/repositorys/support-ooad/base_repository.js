import axios from 'axios'

const httpClient = axios.create({
  baseURL: '/api/product/v2',
  headers: {
    'Content-Type': 'application/json'
    // anything you want to add to the headers
    // Authorization: localStorage.getItem('token')
  }
})

export default httpClient
