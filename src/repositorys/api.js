import repository from './base_repository'

const resource = '/apis'
export default {
  getListAPI (params) {
    // const {} = params
    return repository.get(`${resource}`, { params })
  },
  createApi (payload) {
    const { name, method, path, description } = payload
    return repository.post(`${resource}`, { name, method, path, description })
  },
  updateAPIByID (id, payload) {
    const { name, method, path, description } = payload
    return repository.put(`${resource}/${id}`, { name, method, path, description })
  },
  deleteAPIByID (id) {
    return repository.delete(`${resource}/${id}`)
  }

}
