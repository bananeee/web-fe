export default [
  {
    path: '/editor/uml',
    name: 'UML Editor',
    component: () => import(/* webpackChunkName: "about" */ './view/PlaintUml')
  },
  {
    path: '/editor',
    name: 'UML Editor',
    component: () => import(/* webpackChunkName: "about" */ './view/MDEditor')
  }
]
