import UMLEncoder from 'plantuml-encoder'

export default {
  namespaced: true,
  state: {
    code: '',
    codeRender: '',
    markdown: ''
  },
  mutations: {
    async genUmlImage (state, code) {
      state.code = code
      const umlcode = UMLEncoder.encode(state.code)
      state.codeRender = 'http://149.28.157.34:8081/uml/png/' + umlcode
    },
    async genMarkdown (state, code) {
      state.markdown = code
    }
  },
  actions: {
    async genUmlImage ({ commit, rootState, dispatch }, code) {
      commit('genUmlImage', code)
    },
    async genMarkdown ({ commit, rootState, dispatch }, code) {
      commit('genMarkdown', code)
    }
  }
}
