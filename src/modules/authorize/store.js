const users = [
  {
    user: {
      username: '17020705',
      password: '12345678',
      role: { code: 'student' }
    },
    token: 1
  },

  {
    user: {
      username: '10020701',
      password: '12345678',
      role: { code: 'student' }
    },
    token: 1
  }
]
export default {
  namespaced: true,
  state: {
    user: {}
  },
  mutations: {
    login (state, data, rootState) {
      state.user = data
      // save token
      const { token, user } = data
      localStorage.setItem('token', token)
      localStorage.setItem('user', JSON.stringify(user))
    }
  },
  actions: {
    async login ({ commit, rootState, dispatch }, payload) {
      // login
      const item = users.find(item => item.user.username === payload.username)
      if (item === undefined) {
        rootState.notification.error(
          {
            message: 'Đăng nhập thật bại',
            description:
              'Sai tài khoản hoặc mật khẩu'
          }
        )
      } else {
        commit('login', item, rootState)
      }
      // set state
    }
  }
}
