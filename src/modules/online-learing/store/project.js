
export default {
  namespaced: true,
  state: {
    projects: []
  },
  mutations: {
    login (state, data, rootState) {
      state.user = data
      // save token
      const { token } = data
      localStorage.setItem('token', token)
    }
  },
  actions: {
    async createProject ({ commit, rootState, dispatch }, payload) {

      console.log(this.$notification)
      rootState.notification.success({
        message: 'Notification Title',
        description:
          'This is the content of the notification. This is the content of the notification. This is the content of the notification.'
      })
    },
    async getProjects ({ commit, rootState, dispatch }, params) {

    },
    async updateProject ({ commit, rootState, dispatch }, params) {

    },
    async updateStatusProject ({ commit, rootState, dispatch }, params) {

    }

  }
}
