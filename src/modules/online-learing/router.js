export default [
  {
    path: 'course',
    name: 'ListCourseContainer',
    component: () => import(/* webpackChunkName: "about" */ './view/ListCourseContainer')

  },
  {
    path: 'course/:course_code/students',
    name: 'ListStudentInCourseContainer',
    component: () => import(/* webpackChunkName: "about" */ './view/ListStudentInCourseContainer')
  },
  {
    path: 'course/:course_code/projects',
    name: 'ListProjectContainer',
    component: () => import(/* webpackChunkName: "about" */ './view/ListProjectContainer')
  },
  {
    path: 'course/:course_code/projects',
    name: 'ConfigurationCourseContainer',
    component: () => import(/* webpackChunkName: "about" */ './view/ConfigurationCourseContainer')
  },
  {
    path: 'course/:course_code/groups',
    name: 'ListGroupContainer',
    component: () => import(/* webpackChunkName: "about" */ './view/ListGroupContainer')
  },
  {
    path: '/projects/:project_id',
    name: 'ProjectDetailContainer',
    component: () => import(/* webpackChunkName: "about" */ './view/ProjectDetailContainer')
  },
  {
    path: '/projects/:project_id/use-case/create',
    name: 'CreateUseCaseContainer',
    component: () => import(/* webpackChunkName: "about" */ './view/CreateUseCaseContainer')
  },
  {
    path: 'schedule',
    name: 'Schedule',
    component: () => import(/* webpackChunkName: "about" */ './view/Schedule')
  },
  {
    path: 'use-cases/:id',
    name: 'UseCase',
    component: () => import(/* webpackChunkName: "about" */ './view/UseCaseDetailContainer')
  },
  {
    path: 'test',
    name: 'test',
    component: () => import(/* webpackChunkName: "about" */ './components/use_case_story/ListFlowStep')
  },
  {
    path: 'use-case-story/:use_case_story_id',
    name: 'UseCaseStoryDetail',
    component: () => import(/* webpackChunkName: "about" */ './components/use_case_story/UseCaseStoryDetail')
  },
  {
    path: 'use-case-slices/:use_case_slice_id',
    name: 'UseCaseSliceDetail',
    component: () => import(/* webpackChunkName: "about" */ './view/UseSliceContainer')
  },

  {
    path: 'course/:id',
    name: 'Course Detail',
    component: () => import(/* webpackChunkName: "about" */ './components/course/Course')
  },

  {
    path: 'course/:course_code/teams',
    name: 'ListTeam',
    component: () => import(/* webpackChunkName: "about" */ './components/ListGroup')
  },

  {
    path: 'course/:id/topics',
    name: 'ListTopic',
    component: () => import(/* webpackChunkName: "about" */ './components/course/ListTopic')
  },
  {
    path: 'class',
    name: 'Class',
    component: () => import(/* webpackChunkName: "about" */ './view/ListClassCard')
  },
  {
    path: 'class/:id',
    name: 'ClassByID',
    children: [
      {
        path: 'groups',
        name: 'ListGroup',
        component: () => import(/* webpackChunkName: "about" */ './components/ListGroup')
      },
      {
        path: 'topic',
        name: 'ListTopic',
        component: () => import(/* webpackChunkName: "about" */ './components/course/ListTopic')
      },
      {
        path: 'group/:group_id',
        name: 'MyGroup',
        component: () => import(/* webpackChunkName: "about" */ './view/GroupDetail')
      },
      {
        path: 'lecture',
        name: 'ListLecture',
        component: () => import(/* webpackChunkName: "about" */ './view/ListLecture')
      },
      {
        path: 'group/:group_id',
        name: 'GroupDetail',
        component: () => import(/* webpackChunkName: "about" */ './view/GroupDetail')
      }

    ],
    component: () => import(/* webpackChunkName: "about" */ './view/ClassDetail')
  },

  {
    path: 'student/course/:id',
    name: 'Course Student Detail',
    component: () => import(/* webpackChunkName: "about" */ './view/StudentCourse')
  },
  {
    path: 'group/:group_id',
    name: 'GroupDetail',
    component: () => import(/* webpackChunkName: "about" */ './view/GroupDetail')
  },
  {
    path: 'semester',
    name: 'Semester',
    component: () => import(/* webpackChunkName: "about" */ './view/ListSemester')
  },
  {
    path: 'file',
    name: 'File',
    component: () => import(/* webpackChunkName: "about" */ './components/Lecture')
  },
  {
    path: 'use-case',
    name: 'use-case',
    component: () => import(/* webpackChunkName: "about" */ './components/use_case/ListUseCase')
  },
  {
    path: 'use-cases/:use_case_id',
    name: 'UseCaseDetail',
    component: () => import(/* webpackChunkName: "about" */ './components/use_case/UsecaseDetail')
  },
  {
    path: 'work-packages/:id/use-case-slice/:slice_id',
    name: 'UseCaseSlice',
    component: () => import(/* webpackChunkName: "about" */ './components/use_case_slice/UseCaseSliceDetail')
  }

]
