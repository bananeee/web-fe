import Vue from 'vue'
import { Modal } from 'ant-design-vue'

const ModalCustom = (component) => {
  return Vue.component('ModalCustom', {
    render (creatElement) {
      // const modal = '<a-modal visible={this.visible} title= {this.title} ok={this.handleOk} cancel={this.handleClose}/>'
      return (
        <Modal visible={this.visible} title={this.title} onOk={this.handleOk} onCancel={this.handleClose} maskClosable={false}>
          <component record={{}}></component>

        </Modal>
      )
    },
    data () {
      return {}
    },
    props: {
      visible: Boolean,
      title: String
    },
    methods: {
      handleOk () {
        console.log('ok')
        this.$emit('close', false)
      },
      handleClose () {
        this.$emit('close', false)
      }
    }
  })
}

export default ModalCustom
