import SemesterRepository from '../../repositorys/ooad/semester'
import CourseRepository from '../../repositorys/support-ooad/course'
import TeacherRepository from '../../repositorys/ooad/teacher'
// import StudentRepository from '../../repositorys/ooad/student'
import TopicRepository from '../../repositorys/ooad/topic'
import GroupRepository from '../../repositorys/ooad/group'
import FileRepository from '../../repositorys/file-manager/file'
import IterationRepository from '../../repositorys/ooad/iteration'
import CriteriaRepository from '../../repositorys/ooad/criteria'
import NotificationRepository from '../../repositorys/notification/notification'
import ProjectStore from './store/project'
export default {
  namespaced: true,
  state: {
    user: {}
  },
  mutations: {
    login (state, data, rootState) {
      state.user = data
      // save token
      const { token } = data
      localStorage.setItem('token', token)
    }
  },
  actions: {
    async getListSemester ({ commit, rootState, dispatch }, params) {
      const { data } = await SemesterRepository.getListSemesters(params)
      return data
    },
    async createSemester ({ commit, rootState, dispatch }, paylaod) {
      const { data } = await SemesterRepository.createSemester(paylaod)
      return data
    },
    async updateSemester ({ commit, rootState, dispatch }, { id, payload }) {
      console.log(payload)
      const { data } = await SemesterRepository.updateSemesterByID(id, payload)
      return data
    },
    async updateStatusSemester ({ commit, rootState, dispatch }, { id, status }) {
      const { data } = await SemesterRepository.updateStatusSemesterByID(id, status)
      return data
    },
    async getListCourse ({ commit, rootState, dispatch }, params) {
      const { data } = await CourseRepository.getListCourse(params)
      return data
    },
    async createCourse ({ commit, rootState, dispatch }, payload) {
      const { data } = await CourseRepository.createCourse(payload)
      return data
    },
    async updateClass ({ commit, rootState, dispatch }, { id, payload }) {
      const { data } = await CourseRepository.updateClassByID(id, payload)
      return data
    },
    async updateStatusClass ({ commit, rootState, dispatch }, { id, status }) {
      const { data } = await CourseRepository.updateStatusClassByID(id, { status })
      return data
    },
    async getListTeacher ({ commit, rootState, dispatch }, params) {
      const { data } = await TeacherRepository.getListTeacher({})
      return data
    },
    async getListStudent ({ commit, rootState, dispatch }, id) {
      const { data } = await CourseRepository.getStudentInCourse(id)
      console.log(data)
      return data
    },
    async updateStudentInCourseByID ({ commit, rootState, dispatch }, { id, payload }) {
      const { data } = await CourseRepository.addStudentInClass(id, payload)
      console.log(data)
      return data
    },
    async getClassByID ({ commit, rootState, dispatch }, id) {
      const { data } = await CourseRepository.getClassByID(id)
      console.log(data)
      return data
    },
    async uploadFile ({ commit, rootState, dispatch }, file) {
      const { data } = await FileRepository.uploadFile(file)
      console.log(data)
      return data
    },
    async createTopic ({ commit, rootState, dispatch }, payload) {
      const { data } = await TopicRepository.createTopic(payload)
      return data
    },
    async updateTopic ({ commit, rootState, dispatch }, { id, payload }) {
      const { data } = await TopicRepository.updateTopicByID(id, payload)
      return data
    },
    async getTopicInClass ({ commit, rootState, dispatch }, id) {
      const { data } = await CourseRepository.getProjectInCourse(id)
      return data
    },
    async createGroup ({ commit, rootState, dispatch }, payload) {
      const { data } = await GroupRepository.createGroup(payload)
      return data
    },
    async updateGroup ({ commit, rootState, dispatch }, { id, payload }) {
      const { data } = await GroupRepository.updateGroup(id, payload)
      return data
    },
    async getListGroup ({ commit, rootState, dispatch }, id) {
      const { data } = await CourseRepository.getTeamInCourse(id)
      return data
    },
    async getGroupByID ({ commit, rootState, dispatch }, id) {
      const { data } = await GroupRepository.getGroupByID(id)
      return data
    },
    async getGroupStudent ({ commit, rootState, dispatch }) {
      const { data } = await GroupRepository.getGroupByStudent()
      return data
    },
    async addStudentInGroup ({ commit, rootState, dispatch }, { id, payload }) {
      const { data } = await GroupRepository.addStudentInGroupByID(id, payload)
      return data
    },
    async getListIteration ({ commit, rootState, dispatch }, params) {
      const { data } = await IterationRepository.getListIteration(params)
      return data
    },
    async createIteration ({ commit, rootState, dispatcht }, payload) {
      const { data } = await IterationRepository.createIteration(payload)
      return data
    },
    async addFileInIteration ({ commit, rootState, dispatch }, { id, payload }) {
      const { data } = await IterationRepository.addFileInIteration(id, payload)
      return data
    },
    async getInfoFile ({ commit, rootState, dispatch }, id) {
      const { data } = await FileRepository.getInfoFile(id)
      return data
    },
    async createCriteria ({ commit, rootState, dispatch }, payload) {
      const { data } = await CriteriaRepository.createCriteria(payload)
      return data
    },
    async updateCriteria ({ commit, rootState, dispatch }, { id, payload }) {
      const { data } = await CriteriaRepository.updateCriteria(id, payload)
      return data
    },
    async getListCriteria ({ commit, rootState, dispatch }, params) {
      const { data } = await CriteriaRepository.getListCriteria(params)
      return data
    },
    async addScoreInGroupByID ({ commit, rootState, dispatch }, { id, payload }) {
      const { data } = await GroupRepository.addScoreInGroupByID(id, payload)
      return data
    },
    async createNotification ({ commit, rootState, dispatch }, payload) {
      const { data } = await NotificationRepository.createNotication(payload)
      return data
    },
    async getNotification ({ commit, rootState, dispatch }, params) {
      const { data } = await NotificationRepository.getNotifications(params)
      return data
    }

  },
  modules: {
    projects: ProjectStore
  }
}
