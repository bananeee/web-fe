export default [
  {
    path: 'quizzes',
    name: 'ListQuiz',
    component: () => import(/* webpackChunkName: "about" */ './view/ListQuiz')
  },
  {
    path: 'quizzes/add',
    name: 'QuizCreate',
    component: () => import(/* webpackChunkName: "about" */ './view/CreateQuiz')
  },
  {
    path: 'quizzes/:id',
    name: 'Quiz',
    component: () => import(/* webpackChunkName: "about" */ './view/QuizDetail')
  },
  {
    path: 'quizzes/listen/:id',
    name: 'QuizListen',
    component: () => import(/* webpackChunkName: "about" */ './view/ListenQuiz')
  }

]
