import QuizRepository from '@/repositorys/quiz/quiz'
import QuestionRepository from '@/repositorys/quiz/question'
export default {
  namespaced: true,
  state: {
    user: {}
  },
  mutations: {
    login (state, data, rootState) {
      state.user = data
      // save token
      const { token } = data
      localStorage.setItem('token', token)
    }
  },
  actions: {
    async getLitQuiz ({ commit, rootState, dispatch }, parma) {
      const { data } = await QuizRepository.getListQuiz(parma)
      return data
    },
    async getQuiz ({ commit, rootState, dispatch }, id) {
      const { data } = await QuizRepository.getQuiz(id)
      return data
    },
    async createQuestion ({ commit, rootState, dispatch }, payload) {
      const { data } = await QuestionRepository.createQuestion(payload)
      return data
    },
    async createQuiz ({ commit, rootState, dispatch }, payload) {
      const { data } = await QuizRepository.createQuiz(payload)
      return data
    }

  }
}
