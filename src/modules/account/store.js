import UserRepository from '../../repositorys/user'
import RoleRepository from '../../repositorys/role'
import APIRepository from '../../repositorys/api'

export default {
  namespaced: true,
  state: {
    user: {}
  },
  mutations: {
    login (state, data, rootState) {
      state.user = data
      // save token
      const { token } = data
      localStorage.setItem('token', token)
    }
  },
  actions: {
    async getListUser ({ commit, rootState, dispatch }, params) {
      const { data } = await UserRepository.getListUser(params)
      return data
    },
    async createUser ({ commit, rootState, dispatch }, payload) {
      const { data } = await UserRepository.createUser(payload)
      await dispatch('getListUser', {})
      return data
    },
    async updateUser ({ commit, rootState, dispatch }, { id, payload }) {
      const { data } = await UserRepository.updateUserByID(id, payload)
      await dispatch('getListUser', {})
      return data
    },
    async updateUserStatus ({ commit, rootState, dispatch }, { id, status }) {
      const { data } = await UserRepository.updateStatusUserByID(id, { status })
      await dispatch('getListUser', {})
      return data
    },
    async loginToken ({ dispatch }) {
      const { data } = await UserRepository.loginWithToken()
      if (data.success === true) {
        dispatch('setIsLoggedIn', true, { root: true })
      }
    },
    async getListRole ({ commit, rootState, dispatch }, params) {
      const { data } = await RoleRepository.getListRole(params)
      return data
    },
    async getListAPI ({ commit, rootState, dispatch }, params) {
      const { data } = await APIRepository.getListAPI(params)
      return data
    },
    async createRole ({ commit, rootState, dispatch }, payload) {
      const { data } = await RoleRepository.createRole(payload)
      await dispatch('getListRole', {})
      return data
    },
    async getRoleByID ({ commit, rootState, dispatch }, id) {
      console.log(id)
      const { data } = await RoleRepository.getRoleByID(id)
      return data
    },
    async updateRoleByID ({ commit, rootState, dispatch }, { id, payload }) {
      console.log(id, payload)
      const { data } = await RoleRepository.updateRoleByID(id, payload)
      await dispatch('getListRole', {})
      return data
    },
    async createAPI ({ commit, rootState, dispatch }, payload) {
      const { data } = await APIRepository.createApi(payload)
      await dispatch('getListAPI', {})
      return data
    },
    async updateAPIByID ({ commit, rootState, dispatch }, { id, payload }) {
      const { data } = await APIRepository.updateAPIByID(id, payload)
      await dispatch('getListAPI', {})
      return data
    },
    async deleteAPIByID ({ commit, rootState, dispatch }, id) {
      const { data } = await APIRepository.deleteAPIByID(id)
      await dispatch('getListAPI', {})
      return data
    },
    async addAPIInRoleByID ({ commit, rootState, dispatch }, { id, payload }) {
      const { data } = await RoleRepository.addAPIInRoleByID(id, payload)
      return data
    },
    async deleteInRoleByID ({ commit, rootState, dispatch }, { id, payload }) {
      const { data } = await RoleRepository.remoteAPIInRoleByID(id, payload)
      return data
    }
  }
}
