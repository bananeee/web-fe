export default [
  {
    path: 'users',
    name: 'ListUser',
    component: () => import(/* webpackChunkName: "about" */ './view/ListUser')
  },
  {
    path: 'roles',
    name: 'ListRole',
    component: () => import(/* webpackChunkName: "about" */ './view/ListRole')
  },
  {
    path: 'apis',
    name: 'ListAPI',
    component: () => import(/* webpackChunkName: "about" */ './view/ListAPI')
  }
]
