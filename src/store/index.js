import Vue from 'vue'
import Vuex from 'vuex'
import authorize from '../modules/authorize/store'
import account from '../modules/account/store'
import plaintumlEditor from '../modules/plaintuml-editor/store'
import onlineLearning from '../modules/online-learing/store'
import Quiz from '../modules/quizz/store'

import Repository from '../repositorys/index'

import { notification } from 'ant-design-vue'
Vue.use(Vuex)
Vuex.Store.$repository = Repository
export default new Vuex.Store({
  // plugins: [Repository],
  state: {
    isLoggedIn: false,
    isStudent: false,
    $repository: Repository,

    notification: notification

  },
  getters: {
    getIsLoggedIn (state) {
      return state.isLoggedIn
    }
  },
  mutations: {
    setIsLoggedIn (state, status) {
      state.isLoggedIn = status
    },
    setIsStudent (state, status) {
      state.isStudent = status
    }
  },
  actions: {
    setIsLoggedIn ({ commit }, status) {
      commit('setIsLoggedIn', status)
    },
    isStudent ({ commit }, status) {
      const user = JSON.parse(localStorage.getItem('user'))
      console.log(user.role.code)
      commit('setIsStudent', user.role.code === 'student')
    },
    isTeacher () {
      const user = JSON.parse(localStorage.getItem('user'))
      console.log(user.role.code === 'teacher')
      return user.role.code === 'teacher'
    }
  },
  modules: {
    authorize: authorize,
    account: account,
    plaintUML: plaintumlEditor,
    ooad: onlineLearning,
    quiz: Quiz
  }
})
