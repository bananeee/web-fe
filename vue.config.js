
module.exports = {
  configureWebpack: {
    devtool: 'source-map'
  },
  devServer: {
    proxy: {
      '^/api': {
        target: 'http://127.0.0.1:3000/',
        ws: true,
        changeOrigin: true
      },
      '^/socket.io': {
        target: 'http://127.0.0.1:3000/',
        ws: true,
        changeOrigin: true
      },
      // '^/api': {
      //   target: 'http://127.0.0.1:3000/',
      //   ws: true,
      //   changeOrigin: true
      // },
      // '^/api': {
      //   target: 'http://127.0.0.1:3003/',
      //   ws: true,
      //   changeOrigin: true
      // },
      '^/form': {
        target: 'http://localhost:9002',
        ws: true,
        changeOrigin: true
      },
      '/uml/': {
        target: 'http://localhost:9002',
        ws: true,
        changeOrigin: true
      }
    }
  }
  // plugins: [
  //   new webpack.ProvidePlugin({
  //     jQuery: 'jquery',
  //     $: 'jquery',
  //     'window.jQuery': 'jquery',
  //     'window.$': 'jquery'
  //   })
  // ]
}
