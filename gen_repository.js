let json = {}
const fs = require('fs')
const axios = require('axios')

function genBaseRepository (basePath, folder) {
  const template = `import axios from 'axios'

const httpClient = axios.create({
  baseURL: '${basePath}',
  headers: {
    'Content-Type': 'application/json',
    // anything you want to add to the headers
     Authorization: localStorage.getItem('token')
  }
})

export default httpClient`
  console.log(template)
  fs.writeFileSync(`${folder}/base_repository.js`, template)
}

function genRepositoryPath (paths, folder) {
  // genBaseRepository(json.basePath)
  var keyNames = Object.keys(paths)
  // console.log(keyNames)
  // genFunction(keyNames[3], paths[keyNames[3]])
  let templateGen = ''
  for (const key in keyNames) {
    const pathObject = paths[keyNames[key]]
    for (const method in pathObject) {
      const { summary, parameters } = pathObject[method]
      templateGen = templateGen + genFunction(summary, method, keyNames[key], parameters)
    }
  }
  templateGen = `import repository from './base_repository' \n export default {\n${templateGen}\n}`
  fs.writeFileSync(`${folder}/repository.js`, templateGen)
}

function toCamelCase (str) {
  return str
    .replace(/\s(.)/g, function ($1) {
      return $1.toUpperCase()
    })
    .replace(/\s/g, '')
    .replace(/^(.)/, function ($1) {
      return $1.toLowerCase()
    })
}

function genFunction (name, method, path, parameters) {
  let pathParameters = getPathParameter(parameters)
  let bodyParameters = getBodyParameter(parameters)
  const queryParameters = getQueryParameter(parameters)
  let queryParametersL2 = ''
  parameters = parametersToString(parameters)
  path = path.replace('{', '${')
  if (pathParameters != '') {
    pathParameters = `const ${pathParameters} = path `
  }
  if (queryParameters != '') {
    queryParametersL2 = `const ${queryParameters} = query `
  }
  if (bodyParameters != '') {
    bodyParameters = 'body'
  }
  const nameFunction = toCamelCase(name)
  let config = ''
  if (queryParameters !== '') {
    config = config + queryParameters + ','
  }
  if (bodyParameters !== '') {
    config = config + bodyParameters + ','
  }
  var template = ` ${nameFunction} ${parameters} {
     ${pathParameters}
     ${queryParametersL2}
    return repository.${method}(\`${path}\`,${config} )
  },
  `
  // fs.writeFileSync('test_function.js', template);
  return template
}

function parametersToString (parameters) {
  let parameterString = ''
  let pathParameters = ''
  let bodyParameters = ''
  let queryParameters = ''
  for (i in parameters) {
    const obj = parameters[i]
    if (obj.in === 'path') {
      pathParameters = `${pathParameters} ${obj.name},`
    }
    if (obj.in === 'body') {
      bodyParameters = `${bodyParameters} body`
    }
    if (obj.in === 'query') {
      queryParameters = `${queryParameters} ${obj.name},`
    }
  }
  if (pathParameters !== '') {
    parameterString = `${parameterString} path ,`
  }
  if (queryParameters !== '') {
    parameterString = `${parameterString} query,`
  }
  if (bodyParameters != '') {
    parameterString = `${parameterString} body`
  }

  return `(${parameterString})`
}

function getPathParameter (parameters) {
  let parameterString = ''
  let pathParameters = ''
  // let bodyParameters = ""
  // let queryParameters = ""
  for (i in parameters) {
    const obj = parameters[i]
    if (obj.in === 'path') {
      pathParameters = `${pathParameters} ${obj.name},`
    }
    console.log(obj)
    // if (obj.in === 'body') {
    //     bodyParameters = `${bodyParameters} body`
    // }
    // if (obj.in === 'query') {
    //     queryParameters = `${queryParameters} ${obj.name},`
    // }
  }
  if (pathParameters !== '') {
    parameterString = `${parameterString} {${pathParameters}}`
  }
  console.log(pathParameters)
  return parameterString
}

function getQueryParameter (parameters) {
  let parameterString = ''
  let queryParameters = ''
  // let bodyParameters = ""
  // let queryParameters = ""
  for (i in parameters) {
    const obj = parameters[i]
    if (obj.in === 'query') {
      queryParameters = `${queryParameters} ${obj.name},`
    }
    console.log(obj)
    // if (obj.in === 'body') {
    //     bodyParameters = `${bodyParameters} body`
    // }
    // if (obj.in === 'query') {
    //     queryParameters = `${queryParameters} ${obj.name},`
    // }
  }
  if (queryParameters !== '') {
    parameterString = `${parameterString} {${queryParameters}}`
  }
  console.log(queryParameters)
  return parameterString
}

function getBodyParameter (parameters) {
  let parameterString = ''
  let bodyParameters = ''
  // let bodyParameters = ""
  // let queryParameters = ""
  for (i in parameters) {
    const obj = parameters[i]
    if (obj.in === 'body') {
      bodyParameters = `${bodyParameters} ${obj.name},`
    }
    console.log(obj)
    // if (obj.in === 'body') {
    //     bodyParameters = `${bodyParameters} body`
    // }
    // if (obj.in === 'query') {
    //     queryParameters = `${queryParameters} ${obj.name},`
    // }
  }
  if (bodyParameters !== '') {
    parameterString = `${parameterString} body`
  }
  console.log(bodyParameters)
  return parameterString
}

async function main () {
  const folder = 'test'
  if (!fs.existsSync(`./${folder}`)) {
    fs.mkdirSync(`./${folder}`)
  }
  const { data } = await axios.get('http://localhost:3000/api/support-ooad/v1/swagger/doc.json')
  console.log(data)
  json = data
  genBaseRepository(json.basePath, 'src/repositorys/support-ooad')
  genRepositoryPath(json.paths, 'src/repositorys/support-ooad')
}

main()
